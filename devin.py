def guess_my_number():
    '''
    R0 : “Faire deviner un nombre compris entre 1 et 999 inclus à un utilisateur”
    '''

    to_guess = int(input("J'ai choisi ")) # nombre à deviner

    guesses_number = 0 # nombre de propositions faites par l'utilisateur
    user_guess = 0 # proposition de l'utilisateur

    while to_guess != user_guess:
        
        # Récupérer la proposition de l’utilisateur (user_guess: out)
        user_guess = int(input("Votre proposition : "))

        # Mettre à jour le nombre de propositions faites (guesses_number : in out)
        guesses_number +=1

        # Evaluer sa proposition (user_guess : in, to_guess : in)
        if user_guess > to_guess :
            
            # L’informer que le nombre à deviner est trop grand. (message : out)
            print("Trop grand")

        elif user_guess < to_guess :

            # L’informer que le nombre à deviner est trop petit. (message : out)
            print("Trop petit")

        else :

            # L’informer qu’il a trouvé le bon nombre   (message : out)
            
            print("Trouvé")

    # Afficher un message avec le nombre de propositions quand le nombre est trouvé par l’utilisateur
    print("Félicitation, vous avez trouvé {} en {} essai(s)".format(to_guess, guesses_number))


'''
R0 : “Deviner un nombre compris entre 1 et 999 inclus choisi par un utilisateur”

'''

def make_me_guess():
    '''
R0 : “Deviner un nombre compris entre 1 et 999 inclus choisi par un utilisateur”

    '''

    # Demander à l’utilisateur s’il a choisi un nombre (choice : out, chosen: in out)
    chosen = False
    while not chosen :
        
        choice = input("Avez-vous choisi un nombre entre 1 et 999 (o/n) ? ")

        if choice == "o":
            chosen = True

        else : 
            print("J'attends...")

    guess = 0 # proposition de l'ordinateur
    min = 1 # minimum de l'intervalle possible
    max = 999 # maximum de l'intervalle possible
    guesses_number = 0 # nombre de propositions
    guessed = False # est trouvé?
    props = []

    while not guessed :

        # Vérifier que l’utilisateur n’a pas triché 
        if min <= max :
            print(props)
            # Proposer la médiane de l'intervalle (guess : out)
            guess = (min + max) / 2
            guess = int(guess)
            print("Je propose", guess)

            # Mettre à jour le nombre de propositions 
            guesses_number += 1

            # Faire évaluer ce nombre par l’utilisateur 
            clue = input("Votre indice (t, p, g) : ")

            props.append((clue, guess))
            
            # Redéfinir l’intervalle de réponses possibles (min : out, max : out)
            if clue == "p" :

                # Affecter (guess +1) à min (guess : in, min : out)
                min = guess +1
            
            elif clue == "g" :

                # Affecter (guess - 1) à max (guess : in, max : out)
                max = guess -1

            elif clue == "t" :
                guessed = True
                print("J'ai trouvé {} en {} essai(s)".format(guess, guesses_number))

        else : 
            guessed = True
            print("Vous avez triché !")
            
            user_number = int(input("Quel était votre nombre ? "))
            false_clue = 0
            for number, data in enumerate(props) :
                clue, guess = data
                if clue == "p" :
                    
                    result = "ok" if user_number > guess else "ERREUR"
                    if result == "ERREUR" :
                        false_clue +=1
                    print(" {}: proposition = {}, indice = trop petit : {}".format(number+1, guess, result))

                elif clue == "g":
                    result = "ok" if user_number < guess else "ERREUR"
                    if result == "ERREUR" :
                        false_clue +=1
                    print("Essai {}: votre proposition = {}, indice = trop grand : {}".format(number+1, guess, result))

            print("Vous m'avez donné {} indice(s) erroné(s)".format(false_clue))

make_me_guess()
def ihm() :
    '''
    R0 : “Demander à un utilisateur à quel jeu il veut jouer”
    '''
    not_finished = True
    while not_finished :
        # Afficher les choix possibles pour l’utilisateur

        print("1- L'ordinateur choisit un nombre et vous le devinez", "2- Vous choisissez un nombre et l'ordinateur le devine", "0- Quitter", sep="\n")
        print()
        action = input("Votre choix : ")
        
        

        possible_actions = ("0", "1", "2") 

        while action not in possible_actions :
            
            # Demander à l’utilisateur de choisir une action possible (action : out)
            print()
            print("Je n'ai pas compris votre choix !")
            print()
            print("1- L'ordinateur choisit un nombre et vous le devinez", "2- Vous choisissez un nombre et l'ordinateur le devine", "0- Quitter", sep="\n")
            print()
            action = input("Votre choix : ")
        else : 
            action = int(action)
            

        # lancer l’action demandée

        if action == 1 :
            print()
            guess_my_number()
        
        elif action == 2 :
            print()
            make_me_guess()
        
        else :
            not_finished = False

